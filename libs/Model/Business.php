<?php
namespace Model;

class Business extends \Emagid\Core\Model {
    static $tablename = 'business';
    public static $fields = [
        'name'=>['required'=>true],
        'email'=>['required'=>true,'type'=>'email'],
        'phone'=>['required'=>true],
        'floor'=>['required'=>true],
        'logo',
    ];

    
}