<main>
    <? $questions = $model->questions?>
	<section class="product_page trivia_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
        </header>

        <!-- trivia questions -->
  <section class="trivia_content trivia">
    <div class="title_holder">
      <p>1 / <?=count($questions)?></p>
    </div>

    <!-- question 1 -->
    <?php foreach ($questions as $i => $question) {?>
        <div class="question_holder" data-fail_text="<?=$question->failure_text?>">
            <div class="question">
                <h2>QUESTION <?=$i+1?></h2>
                <p><?=$question->text?></p>
                <? if ($question->image != null && $question->image != '') {?>
                    <img src="<?=UPLOAD_URL . 'questions/' . $question->image ?>" width="100"/>
                <? } ?>
            </div>

            <? $choices = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ?>

            <?foreach ($question->answers as $a => $answer) {?>
            <div class="answer click_action <?=$question->correct_answer_id == $answer->id ? 'correct_answer' : ''?>">
                <h2><?=substr($choices,$a,1)?></h2>
                <p><?=$answer->text?></p>
                <? if ($answer->featured_image != null && $answer->featured_image != '') {?>
                    <img src="<?=UPLOAD_URL . 'answers/' . $answer->featured_image ?>" width="100"/>
                <? } ?>
            </div>
            <? } ?>
            <a class="button submit">NEXT</a>
        </div>
    <? } ?>
  </section>

        
</main>

 