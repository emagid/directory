<?php
 if(count($model->employees)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="15%">Id</th>
            <th width="15%">Image</th>
            <th width="15%">Name</th>
            <th width="20%">Business Name</th>
            <!-- <th width="15%">Email</th>
            <th width="20%">Phone</th>
            <th width="20%">Position</th> -->
            <th width="15%" class="text-center">Edit</th>
            <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->employees as $obj){ ?>
        <tr>
            <td><a href="<?php echo ADMIN_URL; ?>emplyees/update/<?= $obj->id ?>"><?php echo $obj->id; ?></a></td>
            <?$img_path = UPLOAD_URL.'employee/'.$obj->image?>
            <td><a href="<?php echo ADMIN_URL; ?>employees/update/<?= $obj->id ?>"><img src="<?php echo $img_path; ?>" width="50"/></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>emplyees/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>emplyees/update/<?= $obj->id ?>"><?php echo $obj->business_name; ?></a></td>
            <!-- <td><a href="<?php echo ADMIN_URL; ?>emplyees/update/<?= $obj->id ?>"><?php echo $obj->email; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>emplyees/update/<?= $obj->id ?>"><?php echo $obj->phone; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>emplyees/update/<?= $obj->id ?>"><?php echo $obj->position; ?></a></td> -->
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>employees/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>employees/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'business';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>