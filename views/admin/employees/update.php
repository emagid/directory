<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
 
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="test">
        <input type="hidden" name="id" value="<?php echo $model->employee->id; ?>" />
        <input name="token" type="hidden" value="<?php echo get_token();?>" />
        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Name</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>
                    <div class="form-group">
                        <label>Business Name</label>
                        <?php echo $model->form->editorFor("business_name"); ?>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <?php echo $model->form->editorFor("email"); ?>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <?php echo $model->form->editorFor("phone"); ?>
                    </div>
                    <div class="form-group">
                        <label>Position</label>
                        <?php echo $model->form->editorFor("position"); ?>
                    </div>
                    <div class="form-group">
                        <label>Photo</label>
                        <p><input type="file" name="logo" class='image'/></p>

                        <div style="display:inline-block">
                            <?php
                            $img_path = "";
                            if ($model->employee->logo != "" && file_exists(UPLOAD_PATH . 'employee' . DS . $model->employee->image)) {
                                $img_path = UPLOAD_URL . 'employee/' . $model->employee->image;
                                ?>
                                <div class="well well-sm pull-left">
                                    <img src="<?php echo $img_path; ?>" width="100"/>
                                    <br/>
                                    <a href="<?= ADMIN_URL . 'employee/delete_image/' . $model->employee->id; ?>?image=1"
                                       onclick="return confirm('Are you sure?');"
                                       class="btn btn-default btn-xs">Delete</a>
                                    <input type="hidden" name="logo"
                                           value="<?= $model->employee->image ?>"/>
                                </div>
                            <?php } else if($model->employee->image != "") {
                                $img_path = UPLOAD_URL .$model->employee->image; ?>
                                <div class="well well-sm pull-left">
                                    <img src="<?php echo $img_path; ?>" width="100"/>
                                    <br/>
                                    <a href="<?= ADMIN_URL . 'employee/delete_image/' . $model->employee->id; ?>?image=1"
                                       onclick="return confirm('Are you sure?');"
                                       class="btn btn-default btn-xs">Delete</a>
                                    <input type="hidden" name="featured_image1"
                                           value="<?= $model->employee->image ?>"/>
                                </div>
                            <?}?>
                            <div class='preview-container'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 