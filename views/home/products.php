<main>
	<section class="product_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
        </header>

        <!-- background -->
        <div class='inner_page_hero'>
            <div class='overlay'><h1>OUR SERVICES</h1></div>
        </div>

        <!-- home button -->
        <a href="/"><aside id='home_click_white' class='home_click'>
            <img class='white_img' src="<?=FRONT_ASSETS?>img/home.png"> 
        </aside></a>


        <!-- CHOICES -->
            <div class='options products'>
                <a class='option click_action' href="healthcare"><img src="<?=FRONT_ASSETS?>img/healthcare.png"><p>HEALTHCARE</p></a>
                <a class='option click_action' href="privatecloud"><img src="<?=FRONT_ASSETS?>img/private_cloud.png"><p>PRIVATE CLOUD</p></a>
                <a class='option click_action' href="hybrid"><img src="<?=FRONT_ASSETS?>img/hybrid.png"><p>HYBRID IT</p></a>
                <a class='option click_action' href="management"><img src="<?=FRONT_ASSETS?>img/management.png"><p>MANAGEMENT</p></a>
                <a class='option click_action' href="disaster"><img src="<?=FRONT_ASSETS?>img/disaster.png"><p>DISASTER RECOVERY</p></a>
                <a class='option click_action' href="ransomeware"><img src="<?=FRONT_ASSETS?>img/ransomeware.png"><p>RANSOMEWARE</p></a>
                <a class='option click_action' href="ny1"><img src="<?=FRONT_ASSETS?>img/ny1.png"><p>NY1</p></a>
            </div>
        </section>
</main>

 